﻿
#if defined(UNICODE) && !defined(_UNICODE)
#define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
#define UNICODE
#endif

#include <cstdlib>
#include <iostream>
#include <ctime>
#include <random>
#include <tchar.h>
#include <windows.h>
#include <MMSystem.h>
//#pragma comment(lib, "fmodvc.lib")	

#define PRITISNUTO(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
#define BULLETNUM 20;

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure(HWND, UINT, WPARAM, LPARAM);

/*  Make the class name into a global variable  */
TCHAR szClassName[] = _T("CodeBlocksWindowsApp");

LPCWSTR message = L"My First Window";
int hardness = 300;
int hardness2 = 300;
int stoneSpeed = 5;
//CFmod g_Music;
DWORD pocvr;
DWORD pocvr11, pocvr10;
DWORD timering = 0;
DWORD timering2 = 0;
DWORD pocvr2;
DWORD pocvr3;
DWORD pocvr4;
DWORD pocvr5;
int marker = -1;
int marker2 = -1;

int counterBullet = 0;
int counterBullet2 = 0;
int counterStars = 0;
int counterHalley = 0;
void CheckInput(void);
void UpdateBg(HWND);
void UpdateSpaceship(RECT*);
void UpdateSpaceship2(RECT*);
void UpdateBullet(RECT*);
void UpdateBullets(RECT*);
void UpdateStars(RECT*);
void UpdateHalley(RECT*);
void Render(HWND);
void Draw(HDC, RECT*);
BOOL Initialize(void);

const int PAUZA = 20;


int spaceshipX = 0;
int spaceship2X = 0;
int spaceship2Y = 0;
int spaceshipY = 0;
int bulletX = 0;
int bulletY = 0;
int bulletsX = 0;
int bulletsY = 0;
int starsX = 0;
int starsY = 0;
int liveX = 0;
int live2X = 0;
int live2Y = 0;
int liveY = 0;
int signX = 0;
int signY = 0;
int sign2X = 0;
int sign2Y = 0;
int numOfLives = 5;
int numOfLives2 = 5;
int drawen = 0;
int drawen2 = 0;
int halleyX = 0;
int halleyY = 0;


typedef struct Objects
{
	int height;
	int width;
	int x;
	int y;
	int dx;
	int dy;
} Object;

Object spaceship, bullet[20000], bullets[20000], stars[20000], background, lives[5], sign, spaceship2, lives2[5], sign2, halley[2000];

HBITMAP hbmHalley[2000], hbmHalleyMask[2000],hbmSign, hbmSignMask, hbmLives[5], hbmLivesMask[5], hbmSpaceship, hbmSpaceshipMask, hbmSpaceship2, hbmSign2, hbmSign2Mask;
HBITMAP hbmLives2[5], hbmLives2Mask[5],hbmBullet[2000], hbmBulletMask[2000], hbmBullets[2000], hbmBulletsMask[2000], hbmStars[2000], hbmStarsMask[2000], hbmBackground, hbmSpaceship2Mask;



int WINAPI WinMain(HINSTANCE hThisInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpszArgument,
	int nCmdShow)
{
	HWND hwnd;               /* This is the handle for our window */
	MSG messages;            /* Here messages to the application are saved */
	WNDCLASSEX wincl;        /* Data structure for the windowclass */

							 /* The Window structure */
	wincl.hInstance = hThisInstance;
	wincl.lpszClassName = szClassName;
	wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
	wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
	wincl.cbSize = sizeof(WNDCLASSEX);

	/* Use default icon and mouse-pointer */
	wincl.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hCursor = LoadCursor(NULL, IDC_ARROW);
	wincl.lpszMenuName = NULL;                 /* No menu */
	wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
	wincl.cbWndExtra = 0;                      /* structure or the window instance */
											   /* Use Windows's default colour as the background of the window */
	wincl.hbrBackground = (HBRUSH)COLOR_BACKGROUND;

	/* Register the window class, and if it fails quit the program */
	if (!RegisterClassEx(&wincl))
		return 0;



	/* The class is registered, let's create the program*/
	hwnd = CreateWindowEx(
		0,                   /* Extended possibilites for variation */
		szClassName,         /* Classname */
		_T("SpaceX"),       /* Title Text */
		WS_ACTIVECAPTION, /* default window */
		CW_USEDEFAULT,       /* Windows decides the position */
		CW_USEDEFAULT,       /* where the window ends up on the screen */
		1920,             /* The programs width */
		1080,             /* and height in pixels */
		HWND_DESKTOP,        /* The window is a child-window to desktop */
		NULL,                /* No menu */
		hThisInstance,       /* Program Instance handler */
		NULL                 /* No Window Creation data */
	);

	/* Make the window visible on the screen */
	//PlaySound("ItemPickup.wav", NULL, SND_FILENAME | SND_LOOP | SND_ASYNC);
	ShowWindow(hwnd, nCmdShow);

	if (Initialize())
	{
		while (TRUE)
		{
			DWORD vrijeme_pocetak;

			if (PeekMessage(&messages, NULL, 0, 0, PM_REMOVE))
			{
				if (messages.message == WM_QUIT)
				{
					break;
				}
				TranslateMessage(&messages);
				DispatchMessage(&messages);
			}
			vrijeme_pocetak = GetTickCount();
			CheckInput();
			if (PRITISNUTO(VK_ESCAPE))
			{
				HWND hwnd;
				DestroyWindow(hwnd);
				return 0;
			}
			UpdateBg(hwnd);
			Render(hwnd);

			while ((GetTickCount() - vrijeme_pocetak) < PAUZA)
			{
				Sleep(5);
			}
		}
	}

	/* Run the message loop. It will run until GetMessage() returns 0 */
	while (GetMessage(&messages, NULL, 0, 0))
	{
		/* Translate virtual-key messages into character messages */
		TranslateMessage(&messages);
		/* Send message to WindowProcedure */
		DispatchMessage(&messages);
	}

	/* The program return-value is 0 - The value that PostQuitMessage() gave */
	return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)                  /* handle the messages */
	{
	case WM_DESTROY:
		PostQuitMessage(0);       /* send a WM_QUIT to the message queue */
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		return 0;
	default:                      /* for messages that we don't deal with */
		return DefWindowProc(hwnd, message, wParam, lParam);
	}
	

	return 0;
}

BOOL Initialize(void) {

	BITMAP bitmap;

	hbmBackground = (HBITMAP)LoadImage(NULL, "background.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmBackground, sizeof(BITMAP), &bitmap);
	background.width = bitmap.bmWidth;
	background.height = bitmap.bmHeight;
	background.x = 0;
	background.y = 0;
	background.dx = 0;
	background.dy = 0;

	srand(time(0));
	hbmSpaceship = (HBITMAP)LoadImage(NULL, "Spaceship2_black.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSpaceshipMask = (HBITMAP)LoadImage(NULL, "Spaceship2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship, sizeof(BITMAP), &bitmap);
	spaceship.width = bitmap.bmWidth;
	spaceship.height = bitmap.bmHeight;
	spaceship.x = 1000;
	spaceship.y = 430;
	spaceship.dx = 4;
	spaceship.dy = 430;

	srand(time(0));
	hbmSpaceship2 = (HBITMAP)LoadImage(NULL, "spaceshipPlayer2_black.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSpaceship2Mask = (HBITMAP)LoadImage(NULL, "spaceshipPlayer2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSpaceship2, sizeof(BITMAP), &bitmap);
	spaceship2.width = bitmap.bmWidth;
	spaceship2.height = bitmap.bmHeight;
	spaceship2.x = 300;
	spaceship2.y = 430;
	spaceship2.dx = 4;
	spaceship2.dy = 430;

	hbmSign = (HBITMAP)LoadImage(NULL, "sign_black.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSignMask = (HBITMAP)LoadImage(NULL, "sign.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSign, sizeof(BITMAP), &bitmap);
	sign.width = bitmap.bmWidth;
	sign.height = bitmap.bmHeight;
	sign.x = 1500 - 5 * lives[0].width - sign.width - 160;
	sign.y = 30;
	sign.dx = 0;
	sign.dy = 0;

	hbmSign2 = (HBITMAP)LoadImage(NULL, "sign_black.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	hbmSign2Mask = (HBITMAP)LoadImage(NULL, "sign.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmSign2, sizeof(BITMAP), &bitmap);
	sign2.width = bitmap.bmWidth;
	sign2.height = bitmap.bmHeight;
	sign2.x = 300 - 5 * lives[0].width - sign.width - 160;
	sign2.y = 30;
	sign2.dx = 0;
	sign2.dy = 0;





	return TRUE;
}

void Render(HWND hwnd)
{
	RECT clientRectangle;
	HDC hdc = GetDC(hwnd);
	GetClientRect(hwnd, &clientRectangle);
	Draw(hdc, &clientRectangle);
	ReleaseDC(hwnd, hdc);
}

void CheckInput(void)
{
	if (true)
	{
		if (counterStars >= 2000)
		{
			counterStars = 0;
		}
		if (GetTickCount() - pocvr3 >= hardness)
		{
			HWND hwnd;
			BITMAP bitmap, bitmap2;
			hbmStars[counterStars] = (HBITMAP)LoadImage(NULL, "comet_black.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			hbmStarsMask[counterStars] = (HBITMAP)LoadImage(NULL, "comet.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			GetObject(hbmStars[counterStars], sizeof(BITMAP), &bitmap);
			stars[counterStars].width = bitmap.bmWidth;
			stars[counterStars].height = bitmap.bmHeight;
 			stars[counterStars].x = rand() % (1920);
			stars[counterStars].y = 0;
			stars[counterStars].dx = 0;
			if (counterStars % (rand() % (4) + 1) == 0)
				stars[counterStars].dy = 20;
			else if (counterStars % 10 == 0)
				stars[counterStars].dy = 30;
			else
			{
				stars[counterStars].dy = 40;
			}
			counterStars++;
			hardness -= 1;
			stoneSpeed++;


			if (hardness == 0)
			{
				hardness =200;
			}

        pocvr3 = GetTickCount();
			pocvr5 = GetTickCount();

		}

		if (counterHalley >= 2000)
		{
			counterHalley = 0;
		}
		if (GetTickCount() - pocvr10 >= hardness2)
		{
			HWND hwnd;
			BITMAP bitmap2;
			hbmHalley[counterHalley] = (HBITMAP)LoadImage(NULL, "comett_black.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			hbmHalleyMask[counterHalley] = (HBITMAP)LoadImage(NULL, "comett.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			GetObject(hbmHalley[counterHalley], sizeof(BITMAP), &bitmap2);
			halley[counterHalley].width = bitmap2.bmWidth;
			halley[counterHalley].height = bitmap2.bmHeight;
			halley[counterHalley].x = rand() % (1920)+50;
			halley[counterHalley].y = 0;
			halley[counterHalley].dx = 0;
			halley[counterHalley].dy = 30;
			counterHalley++;
			hardness2 -= 1;
			if (hardness2 == 0)
			{
				hardness2 = 200;
			}

			pocvr10 = GetTickCount();
			pocvr11 = GetTickCount();

		}

	

		if (drawen < numOfLives)
		{
			BITMAP bitmap;
			hbmLives[drawen] = (HBITMAP)LoadImage(NULL, "lives_black.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			hbmLivesMask[drawen] = (HBITMAP)LoadImage(NULL, "lives.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			GetObject(hbmLives[drawen], sizeof(BITMAP), &bitmap);
			lives[drawen].width = bitmap.bmWidth;
			lives[drawen].height = bitmap.bmHeight;
			lives[drawen].x = 1500 - (drawen + 1)*lives[drawen].width;
			lives[drawen].y = 30;
			lives[drawen].dx = 0;
			lives[drawen].dy = 0;
			drawen++;
		}

		if (drawen2 < numOfLives2)
		{
			BITMAP bitmap;
			hbmLives2[drawen2] = (HBITMAP)LoadImage(NULL, "lives_black.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			hbmLives2Mask[drawen2] = (HBITMAP)LoadImage(NULL, "lives.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			GetObject(hbmLives2[drawen2], sizeof(BITMAP), &bitmap);
			lives2[drawen2].width = bitmap.bmWidth;
			lives2[drawen2].height = bitmap.bmHeight;
			lives2[drawen2].x = 300 - (drawen2 + 1)*lives2[drawen2].width;
			lives2[drawen2].y = 30;
			lives2[drawen2].dx = 0;
			lives2[drawen2].dy = 0;
			drawen2++;
		}
	}

	
	if (PRITISNUTO(VK_RIGHT))
	{
		spaceship.x += 14;;
		if (spaceship.x > (1800-spaceship.width/2))
			spaceship.x = -spaceship.width/2;
	}
	if (PRITISNUTO(0x44))
	{
		spaceship2.x += 14;;
		if (spaceship2.x > (1800 - spaceship2.width / 2))
			spaceship2.x = -spaceship2.width / 2;
	}
	if (PRITISNUTO(VK_LEFT))
	{
		spaceship.x -= 14;;
		if (spaceship.x < -spaceship.width/2)
			spaceship.x =1800-spaceship.width/2;
	}
	if (PRITISNUTO(0x41))
	{
		spaceship2.x -= 14;;
		if (spaceship2.x < -spaceship2.width / 2)
			spaceship2.x = 1800 - spaceship2.width / 2;
	}
	if (PRITISNUTO(VK_UP))
	{
		if (spaceship.y>0)
		spaceship.y -= 14;;

	}
	if (PRITISNUTO(0X57))
	{
		if (spaceship2.y > 0)
		spaceship2.y -= 14;
	}
	if (PRITISNUTO(VK_DOWN))
	{
		if (spaceship.y<900-spaceship.height)
		spaceship.y += 14;
	}
	if (PRITISNUTO(0x53))
	{
		if (spaceship2.y<900 - spaceship2.height)
			spaceship2.y += 14;
	}

	if (PRITISNUTO(VK_SPACE))
	{
		if (counterBullet >= 2000)
		{
			counterBullet = 0;
		}
		if (GetTickCount() - pocvr > 200)
		{

			BITMAP bitmap;
			hbmBullet[counterBullet] = (HBITMAP)LoadImage(NULL, "beam_black.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			hbmBulletMask[counterBullet] = (HBITMAP)LoadImage(NULL, "beam.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			GetObject(hbmBullet[counterBullet], sizeof(BITMAP), &bitmap);
			bullet[counterBullet].width = bitmap.bmWidth;
			bullet[counterBullet].height = bitmap.bmHeight;
			bullet[counterBullet].x = spaceship2.x + (spaceship2.width - bullet->width) / 2;
			bullet[counterBullet].y = spaceship2.y - (bullet->height)+50;
			bullet[counterBullet].dx = 8;
			bullet[counterBullet].dy = 15;
			counterBullet++;
			PlaySound("Click05 .wav", NULL, SND_FILENAME | SND_ASYNC);
			pocvr = GetTickCount();
		}


	}
	if (PRITISNUTO(VK_RSHIFT))
	{
		if (counterBullet2 >= 2000)
		{
			counterBullet2 = 0;
		}
		if (GetTickCount() - pocvr2 > 200)
		{
			BITMAP bitmap;
			hbmBullets[counterBullet2] = (HBITMAP)LoadImage(NULL, "beam_black.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			hbmBulletsMask[counterBullet2] = (HBITMAP)LoadImage(NULL, "beam.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			GetObject(hbmBullets[counterBullet2], sizeof(BITMAP), &bitmap);
			bullets[counterBullet2].width = bitmap.bmWidth/3;
			bullets[counterBullet2].height = bitmap.bmHeight;
			bullets[counterBullet2].x = spaceship.x + (spaceship.width - bullets->width) / 2;
			bullets[counterBullet2].y = spaceship.y - (bullets->height) + 40;
			bullets[counterBullet2].dx = 8;
			bullets[counterBullet2].dy = 15;
			counterBullet2++;
			PlaySound("Death.wav", NULL, SND_FILENAME | SND_ASYNC);
			pocvr2 = GetTickCount();
		}
	}

}

void UpdateBg(HWND hwnd)
{
	pocvr4 = GetTickCount();
	RECT clientRectangle;
	GetClientRect(hwnd, &clientRectangle);
	UpdateSpaceship(&clientRectangle);
	UpdateSpaceship2(&clientRectangle);
	UpdateBullet(&clientRectangle);
	UpdateBullets(&clientRectangle);
	UpdateHalley(&clientRectangle);
	UpdateStars(&clientRectangle);
	for (int k = 0; k < counterStars; k++)
	{
		if (stars[k].x < spaceship.x + spaceship.width &&
			stars[k].x + stars[k].width > spaceship.x &&
			stars[k].y < spaceship.y + spaceship.height - 50 &&
			stars[k].height + stars[k].y > spaceship.y + 20)
		{
			if (GetTickCount() - timering > 2500)
			{
				if (numOfLives == 0)
					DestroyWindow(hwnd);
				else if (marker != k)
				{
					numOfLives--;
					marker = k;
					stars[k].x = 2000;
					timering = GetTickCount();
					PlaySound("BlowDistanced.wav", NULL, SND_FILENAME | SND_ASYNC);
					break;
				}
			}
		}
		if (stars[k].x < spaceship2.x + spaceship2.width &&
			stars[k].x + stars[k].width > spaceship2.x &&
			stars[k].y < spaceship2.y + spaceship2.height - 50 &&
			stars[k].height + stars[k].y > spaceship2.y + 20)
		{
			if (GetTickCount() - timering > 2500)
			{
				if (numOfLives2 == 0)
				{
					DestroyWindow(hwnd);
				}
				else if (marker2 != k)
				{
					numOfLives2--;
					marker2 = k;
					stars[k].x = 2000;
					timering2 = GetTickCount();
					break;
				}
			}
		}
	}
	for (int k = 0; k < counterStars; k++)
	{
		for (int j = 0; j < counterBullet; j++)
		{
			if (stars[k].x < bullet[j].x + bullet[j].width &&
				stars[k].x + stars[k].width > bullet[j].x &&
				stars[k].y < bullet[j].y + bullet[j].height &&
				stars[k].height + stars[k].y > bullet[j].y)
			{
				stars[k].x = 2000;
				bullet[j].x = 2000;
			}
		}
		for (int m = 0; m < counterBullet2; m++)
		{
			if ((stars[k].x < bullets[m].x + bullets[m].width &&
				stars[k].x + stars[k].width > bullets[m].x &&
				stars[k].y < bullets[m].y + bullets[m].height &&
				stars[k].height + stars[k].y > bullets[m].y))
			{
				stars[k].x = 2000;
				bullets[m].x = 2000;
			}
		}
	}
}

void UpdateSpaceship(RECT* prect) {

}

void UpdateSpaceship2(RECT* prect) {

}


void UpdateBullet(RECT* prect) {

	for (int i = 0; i < counterBullet; i++)
	{
		bullet[i].y -= bullet[i].dy;
		if (bullet[i].y == 0)
			marker2 = true;
	}
}

void UpdateStars(RECT* prect)
{
	for (int i = 0; i < counterStars; i++)
	{
		stars[i].y += stars[i].dy;
	}
}

void UpdateHalley(RECT* prect)
{
	for (int i = 0; i < counterHalley; i++)
	{
		halley[i].y += halley[i].dy;
	}
}

void UpdateBullets(RECT* prect) {

	for (int i = 0; i < counterBullet2; i++)
	{
		bullets[i].y -= bullets[i].dy;
		if (bullet[i].y == 0)
			marker = true;
	}
	}



void Draw(HDC hdc, RECT* prect)
{


	HDC hdcBuffer = CreateCompatibleDC(hdc);
	HBITMAP hbmBuffer = CreateCompatibleBitmap(hdc, prect->right, prect->bottom);
	HBITMAP hbmOldBuffer = (HBITMAP)SelectObject(hdcBuffer, hbmBuffer);

	HDC hdcMem = CreateCompatibleDC(hdc);

	HBITMAP hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBackground);
	BitBlt(hdcBuffer, background.x, background.y, background.width, background.height, hdcMem, 0, 0, SRCCOPY);
	SelectObject(hdc, hbmOld);

	(HBITMAP)SelectObject(hdcMem, hbmSpaceshipMask);
	BitBlt(hdcBuffer, spaceship.x, spaceship.y, spaceship.width, spaceship.height, hdcMem, spaceshipX*spaceship.width, spaceshipY*spaceship.height, SRCAND);

	HBITMAP hbmOld1 = (HBITMAP)SelectObject(hdcMem, hbmSpaceship);
	BitBlt(hdcBuffer, spaceship.x, spaceship.y, spaceship.width, spaceship.height, hdcMem, spaceshipX*spaceship.width, spaceshipY*spaceship.height, SRCPAINT);

	(HBITMAP)SelectObject(hdcMem, hbmSpaceship2Mask);
	BitBlt(hdcBuffer, spaceship2.x, spaceship2.y, spaceship2.width, spaceship2.height, hdcMem, spaceship2X*spaceship2.width, spaceship2Y*spaceship2.height, SRCAND);
	
	HBITMAP hbmOld10 = (HBITMAP)SelectObject(hdcMem, hbmSpaceship2);
	BitBlt(hdcBuffer, spaceship2.x, spaceship2.y, spaceship2.width, spaceship2.height, hdcMem, spaceship2X*spaceship2.width, spaceship2Y*spaceship2.height, SRCPAINT);
	
	(HBITMAP)SelectObject(hdcMem, hbmSignMask);
	BitBlt(hdcBuffer, sign.x, sign.y, sign.width, sign.height, hdcMem, signX*sign.width, signY*sign.height, SRCAND);

	HBITMAP hbmOld2 = (HBITMAP)SelectObject(hdcMem, hbmSign);
	BitBlt(hdcBuffer, sign.x, sign.y, sign.width, sign.height, hdcMem, signX*sign.width, signY*sign.height, SRCPAINT);

	(HBITMAP)SelectObject(hdcMem, hbmSign2Mask);
	BitBlt(hdcBuffer, sign2.x, sign2.y, sign2.width, sign2.height, hdcMem, sign2X*sign.width, sign2Y*sign.height, SRCAND);

	HBITMAP hbmOld20 = (HBITMAP)SelectObject(hdcMem, hbmSign2);
	BitBlt(hdcBuffer, sign2.x, sign2.y, sign2.width, sign2.height, hdcMem, sign2X*sign2.width, sign2Y*sign2.height, SRCPAINT);

	for (int i = 0; i < counterBullet; i++)
	{
		(HBITMAP)SelectObject(hdcMem, hbmBulletMask[i]);
		BitBlt(hdcBuffer, bullet[i].x, bullet[i].y, bullet[i].width, bullet[i].height, hdcMem, bulletX*bullet[i].width, bulletY*bullet[i].height, SRCAND);

		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBullet[i]);
		BitBlt(hdcBuffer, bullet[i].x, bullet[i].y, bullet[i].width, bullet[i].height, hdcMem, bulletX*bullet[i].width, bulletY*bullet[i].height, SRCPAINT);
	}

	for (int i = 0; i < numOfLives; i++)
	{
		(HBITMAP)SelectObject(hdcMem, hbmLivesMask[i]);
		BitBlt(hdcBuffer, lives[i].x, lives[i].y, lives[i].width, lives[i].height, hdcMem, liveX*lives[i].width, liveY*lives[i].height, SRCAND);

		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmLives[i]);
		BitBlt(hdcBuffer, lives[i].x, lives[i].y, lives[i].width, lives[i].height, hdcMem, liveX*lives[i].width, liveY*lives[i].height, SRCPAINT);
	}

	for (int i = 0; i < numOfLives2; i++)
	{
		(HBITMAP)SelectObject(hdcMem, hbmLives2Mask[i]);
		BitBlt(hdcBuffer, lives2[i].x, lives2[i].y, lives2[i].width, lives2[i].height, hdcMem, live2X*lives2[i].width, live2Y*lives2[i].height, SRCAND);

		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmLives[i]);
		BitBlt(hdcBuffer, lives2[i].x, lives2[i].y, lives2[i].width, lives2[i].height, hdcMem, live2X*lives2[i].width, live2Y*lives2[i].height, SRCPAINT);
	}



	for (int i = 0; i < counterBullet2; i++)
	{
		(HBITMAP)SelectObject(hdcMem, hbmBulletsMask[i]);
		BitBlt(hdcBuffer, bullets[i].x, bullets[i].y, bullets[i].width, bullets[i].height, hdcMem, bulletsX*bullets[i].width, bulletsY*bullets[i].height, SRCAND);

		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmBullets[i]);
		BitBlt(hdcBuffer, bullets[i].x, bullets[i].y, bullets[i].width, bullets[i].height, hdcMem, bulletsX*bullets[i].width, bulletsY*bullets[i].height, SRCPAINT);
	}

	for (int i = 0; i < counterStars; i++)
	{
		(HBITMAP)SelectObject(hdcMem, hbmStarsMask[i]);
		BitBlt(hdcBuffer, stars[i].x, stars[i].y, stars[i].width, stars[i].height, hdcMem, starsX*stars[i].width, starsY*bullets[i].height, SRCAND);

		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmStars[i]);
		BitBlt(hdcBuffer, stars[i].x, stars[i].y, stars[i].width, stars[i].height, hdcMem, starsX*stars[i].width, starsY*stars[i].height, SRCPAINT);
	}

/*	for (int i = 0; i < counterHalley; i++)
	{
		(HBITMAP)SelectObject(hdcMem, hbmHalleyMask[i]);
		BitBlt(hdcBuffer, halley[i].x, halley[i].y, halley[i].width, halley[i].height, hdcMem, halleyX*halley[i].width, halleyY*halley[i].height, SRCAND);

		hbmOld = (HBITMAP)SelectObject(hdcMem, hbmStars[i]);
		BitBlt(hdcBuffer, halley[i].x, halley[i].y, halley[i].width, halley[i].height, hdcMem, halleyX*halley[i].width, halleyY*halley[i].height, SRCPAINT);
	 } */

	halleyY++;
	spaceshipY++;
	bulletY++;
	bulletsY++;
	starsY++;
	liveY++;
	signY++;
	sign2Y++;
	spaceship2Y++;
	if (halleyY == 1)
	{
		halleyY = 0;
	}
	if (spaceshipY == 1)
	{
		spaceshipY = 0;
	}
	if (spaceship2Y == 1)
	{
		spaceship2Y = 0;
	}
	if (bulletY == 1)
	{
		bulletY = 0;
	}
	if (bulletsY == 1)
	{
		bulletsY = 0;
	}
	if (starsY == 1)
	{
		starsY = 0;
	}
	if (liveY == 1)
	{
		liveY = 0;
	}
	if (signY == 1)
	{
		signY = 0;
	}
	if (sign2Y == 1)
	{
		sign2Y = 0;
	}

	BitBlt(hdc, 0, 0, prect->right, prect->bottom, hdcBuffer, 0, 0, SRCCOPY);

	SelectObject(hdcMem, hbmOld);

	DeleteObject(hdcMem);
	DeleteObject(hbmBuffer);

	SelectObject(hdcBuffer, hbmOldBuffer);
	DeleteObject(hdcBuffer);
	DeleteObject(hbmOldBuffer);
	DeleteObject(hbmBuffer);

}

